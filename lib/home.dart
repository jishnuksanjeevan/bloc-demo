import 'package:bloc_tutorial/counter_bloc.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  final String title;
  const HomePage({Key? key, required this.title}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  CounterBloc counterBloc = CounterBloc();
  int _counter = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Text(
            'Counter',
            style: TextStyle(fontSize: 30),
          ),
          StreamBuilder(
              stream: counterBloc.counterStream,
              initialData: 0,
              builder: (context, snapshot) {
                return Text(
                  '${snapshot.data}',
                  style: const TextStyle(fontSize: 30),
                );
              }),
          TextButton(
            onPressed: () {
              counterBloc.eventSink.add(CounterEvents.Increment);
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Text('Add'),
                Icon(Icons.add),
              ],
            ),
          ),
          TextButton(
            onPressed: () => counterBloc.eventSink.add(CounterEvents.Decrement),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Text('Minus'),
                Icon(Icons.remove),
              ],
            ),
          ),
          TextButton(
            onPressed: () => counterBloc.eventSink.add(CounterEvents.Reset),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Text('Reset'),
                Icon(Icons.loop),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

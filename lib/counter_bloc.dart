import 'dart:async';

enum CounterEvents { Increment, Decrement, Reset }

class CounterBloc {
  int counter = 0;
  final _stateStreamController = StreamController<int>();

  StreamSink<int> get counterSink => _stateStreamController.sink;
  Stream<int> get counterStream => _stateStreamController.stream;

  final _eventStreamController = StreamController<CounterEvents>();
  StreamSink<CounterEvents> get eventSink => _eventStreamController.sink;
  Stream<CounterEvents> get eventStream => _eventStreamController.stream;

  CounterBloc() {
    counter = 0;
    eventStream.listen((event) {
      if (event == CounterEvents.Increment) {
        counter++;
      } else if (event == CounterEvents.Decrement) {
        counter--;
      } else if (event == CounterEvents.Reset) {
        counter = 0;
      }
      counterSink.add(counter);
    });
  }
}
